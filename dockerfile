FROM ubuntu:16.04
MAINTAINER Dmytro Savinov <keppnew@gmail.com>

RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" > /etc/apt/sources.list.d/pgdg.list

RUN apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8

RUN apt-get update && apt-get install -y postgresql-9.6 postgresql-contrib-9.6


USER postgres


RUN /etc/init.d/postgresql start &&\
    psql --command "CREATE USER keppnew WITH SUPERUSER PASSWORD 'keppnew';" &&\
    createdb -O keppnew todo_list


RUN echo "host todo_list  keppnew    0.0.0.0/0  md5" >> /etc/postgresql/9.6/main/pg_hba.conf

RUN echo "listen_addresses='*'" >> /etc/postgresql/9.6/main/postgresql.conf

ENV PGDATA /var/lib/postgresql/data
RUN mkdir -p "$PGDATA"
VOLUME /var/lib/postgresql/data

EXPOSE 5432


CMD ["/usr/lib/postgresql/9.6/bin/postgres", "-D", "/var/lib/postgresql/data", "-c", "config_file=/etc/postgresql/9.6/main/postgresql.conf"]